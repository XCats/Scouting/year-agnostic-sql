SELECT
  w.team_num,
  UNNEST(ARRAY['wins', 'losses','ties']) as values,
  UNNEST(ARRAY[w.wins,
  l.losses,
  t.ties]) as count
FROM (
  SELECT
    tm.team_num AS team_num,
    COUNT(*) AS wins
  FROM
    team_match tm
    JOIN match_result mr ON tm.match_id = mr.match_id
    AND tm.alliance = mr.winner
  GROUP BY
    tm.team_num) w
  LEFT OUTER JOIN (
    SELECT
      tm.team_num AS team_num,
      COUNT(*) AS losses
    FROM
      team_match tm
      JOIN match_result mr ON tm.match_id = mr.match_id
      AND tm.alliance != mr.winner
      AND mr.winner != 'tie'
    GROUP BY
      tm.team_num) l ON w.team_num = l.team_num
  LEFT OUTER JOIN (
    SELECT
      tm.team_num AS team_num,
      COUNT(*) AS ties
    FROM
      team_match tm
      JOIN match_result mr ON tm.match_id = mr.match_id
      AND mr.winner = 'tie'
    GROUP BY
      tm.team_num) t ON w.team_num = t.team_num 
  WHERE t.ties > 0
  ORDER BY
    w.team_num
