-- General (year-agnostic) tables

CREATE SCHEMA frc2018; -- Create schema for 2018 season

SET search_path TO frc2018; -- Change schema for 2018 season

-- Keep track of events
CREATE TABLE event (
    event_key   varchar PRIMARY KEY
,   event_name  varchar
,   start_date  DATE
,   end_date    DATE
);

-- Create table of teams
CREATE TABLE team (
    team_num    integer PRIMARY KEY
,   name        varchar(500)
,   team_key    varchar(7) NOT NULL UNIQUE
);

-- Keep track of teams at events
CREATE TABLE event_team (
    event_key   varchar REFERENCES event (event_key)
,   team_num    integer REFERENCES team (team_num)
,   CONSTRAINT event_team_num PRIMARY KEY (event_key, team_num)
);

-- Create match table, to autofill
CREATE TABLE match (
    match_id    SERIAL PRIMARY KEY
,   match_level varchar(2) NOT NULL
,   match_num   integer NOT NULL
,   match_set   integer DEFAULT 1
,   event_key   varchar REFERENCES event (event_key)
,   UNIQUE (
            match_num
        ,   match_level
        ,   match_set
        ,   event_key
        )
);

--Restart match_id sequence at 1000 (to avoid confusion with match number)
ALTER SEQUENCE match_match_id_seq RESTART WITH 1000;

-- Use this ALAD for match timings (use for FRC Season, not needed for EE)
-- CREATE TABLE match_times (
--     match_id        integer REFERENCES match (match_id) PRIMARY KEY
-- ,   scheduled_time  DATE NOT NULL
-- ,   predicted_time  DATE
-- ,   actual_time     DATE
-- );
--
-- Maybe use this ALAD to relate match levels to a table?
-- CREATE TABLE match_level (
--     name    varchar(2) PRIMARY KEY,
-- );

-- Create table for match results
CREATE TABLE match_result (
    match_id    integer REFERENCES match (match_id) PRIMARY KEY
,   winner      varchar NOT NULL CHECK (winner = 'red'
        OR winner = 'blue'
        OR winner = 'tie')
,   red_points  integer NOT NULL
,   blue_points integer NOT NULL
);

-- Create table to hold teams for each match
-- Driver station position, alliance
CREATE TABLE team_match (
    match_id        integer REFERENCES match (match_id)
,   team_num        integer NOT NULL REFERENCES team (team_num)
,   alliance        varchar(4) CHECK (alliance = 'red' OR alliance = 'blue')
,   driver_station  integer NOT NULL CHECK (driver_station <= 3 AND driver_station > 0)
,   CONSTRAINT match_team PRIMARY KEY (match_id, team_num)
,   CONSTRAINT match_alliance_driver UNIQUE (match_id, alliance, driver_station)
);